# TODO: Анализ количества недель, прошедших с крайней даты
import sqlite3
import date_check
import id_check
import telebot
from telebot import types

import config

bot = telebot.TeleBot(config.token)


@bot.message_handler(commands=['start'])
def caves_bot(message):
    markup_syan = types.ReplyKeyboardMarkup()
    markup_syan.row('Расписание 439', 'Карта Сьян', 'Кто на заброс [TEST]')
    bot.send_message(message.chat.id, u'\U0001F526' + ' Добрейшего-моднейшего! ' + u'\U0001F526' + '\n'
                                      'Сьянобот на связи.\n\nСообщества Сьян Вконтакте:\nhttps://vk.com/club1285153'
                                      '\nhttps://vk.com/club11726366', reply_markup=markup_syan)

    @bot.message_handler(func=lambda message: message.text == 'Расписание 439')
    def bus(message):
        markup_bus_schedule = types.ReplyKeyboardMarkup()
        markup_bus_schedule.row('В Сьяны', 'Из Сьян')
        bot.send_message(message.chat.id, 'Актуальная стоимость проезда - 69 р.\n\n'
                                          'Выберите направление', reply_markup=markup_bus_schedule)

        @bot.message_handler(func=lambda message: message.text == 'В Сьяны')
        def bus_to_caves(message):
            f = open(config.to)
            bot.send_message(message.chat.id, f.read())
            f.close()

        @bot.message_handler(func=lambda message: message.text == 'Из Сьян')
        def bus_to_caves(message):
            f = open(config.back)
            bot.send_message(message.chat.id, f.read())
            f.close()

    @bot.message_handler(func=lambda message: message.text == 'Карта Сьян')
    def caves_map(message):
        map_download = types.InlineKeyboardMarkup()
        url_button = types.InlineKeyboardButton(text="Открыть ссылку на карту",
                                                url="https://netpics.org/images/2017/12/12/GqPxs.png")
        map_download.add(url_button)
        bot.send_message(message.chat.id, "Карту системы Вы можете скачать по ссылке ниже", reply_markup=map_download)

    @bot.message_handler(func=lambda message: message.text == 'Кто на заброс [TEST]')
    def weekend_plan(message):
        markup_weekend = types.ReplyKeyboardMarkup()
        markup_weekend.row('Кто едет в ближайшие выходные', 'Я еду на заброс!')

        bot.send_message(message.chat.id, "Теперь не нужно засорять стену в группе в ВК!\n"
                                          "Здесь вы можете посмотреть, кто едет в Систему на ближайшие "
                                          "выходные и добавить себя в этот список.", reply_markup=markup_weekend)
        date_check.check()

        @bot.message_handler(func=lambda message: message.text == 'Кто едет в ближайшие выходные')
        def who_is_in(message):
            conn = sqlite3.connect(config.db)
            cursor = conn.cursor()

            is_in = ''
            for item in cursor.execute("SELECT weekend FROM list").fetchall():
                is_in = is_in + str(item)[2:len(str(item))-3] + '\n'
            if (is_in == ''):
                bot.send_message(message.chat.id, 'Пока никто не отметился.')
            else:
                bot.send_message(message.chat.id, is_in)
            conn.close()

        @bot.message_handler(func=lambda message: message.text == 'Я еду на заброс!')
        def i_am_in(message):
            if(id_check.check(message.chat.id)):
                bot.send_message(message.chat.id, 'Вы уже записаны')
            else:
                bot.send_message(message.chat.id, 'Введите подземное имя')

                @bot.message_handler(content_types=["text"])
                def insert_name(message):
                    conn = sqlite3.connect(config.db)
                    cursor = conn.cursor()
                    cursor.execute('INSERT INTO list VALUES (?, ?)', [message.text, message.chat.id])
                    conn.commit()
                    conn.close()


if __name__ == '__main__':
    bot.polling(none_stop=True)
