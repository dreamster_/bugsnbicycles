import datetime
import shelve
import sqlite3

import config


def check():
    d = shelve.open(config.date, flag="c")
    if "next" in d:
        if datetime.date.today() > d["next"]:
            d["next"] += datetime.timedelta(days=7)
            d.close()

            conn = sqlite3.connect(config.db)
            cursor = conn.cursor()
            cursor.execute("DELETE FROM list")
            conn.commit()
            conn.close()
    else:
        d["next"] = datetime.date.today()
        d.close()
